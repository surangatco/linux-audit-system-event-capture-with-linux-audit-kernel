#include <iostream>
#include <stdio.h>
#include <linux/audit.h>
#include <libaudit.h>
#include <string>
#include <ev.h>
#include <unistd.h>


#define AUDIT_KEY "key=SURA"

using namespace std;

int gi_AuditFD=-1;

int checkAuditReadiness(int iFD)
{
	int iRetval = audit_is_enabled(iFD);

	if (iRetval == 1) 
	{
		return 0;
	} 
	else if (iRetval == 0) 
	{
		cout << "Kernel Audit support not enabled. enabling..." << endl;
		if (audit_set_enabled(iFD, 1) < 0) 
		{
			cout << "Kernel Audit support enable error" << endl;
			return -1;
		}
		cout << "Kernel Audit support enable success." << endl;
		return 0;
	} 
	else if (iRetval == -1) 
	{
		cout << "Failed checking audit status" << endl;
		return -1;
	} 
	else 
	{
		cout << "Unknown error" << endl;
		return -1;
	}
	return -1;
}


bool sendRuleData(int iAuditFD, const std::string& rDirectory)
{
	struct audit_rule_data* pRule = NULL;
	unsigned int iFlagVals = 0;
	int iRetval = -1;


	pRule = new audit_rule_data();
	if (pRule == NULL)
	{
		cout << "Failed - create new rule object"<< endl;
		return false;
	}

	iRetval = audit_add_watch_dir(AUDIT_WATCH, &pRule, rDirectory.c_str());

	if (iRetval == -1)
	{
		cout << "Failed - adding watch name"<< endl;
		return false;
	}

	iFlagVals |= AUDIT_PERM_WRITE;
	iFlagVals |= AUDIT_PERM_ATTR;
	iRetval = audit_update_watch_perms(pRule, iFlagVals);
 
	if (iRetval < 0)
	{
		cout << "Failed - Can not update watch params" << endl;
		return false;
	}

	iFlagVals = 0;
	iFlagVals = AUDIT_FILTER_EXIT & AUDIT_FILTER_MASK;
	std::string sKey = AUDIT_KEY;

	cout << sKey << endl;

	iRetval = audit_rule_fieldpair_data(&pRule, sKey.c_str(), iFlagVals);


	if (iRetval < 0)
	{
		cout << "Failed - paring rule data ["<< errno << "]" << endl;
		return false;
	}

	iRetval = audit_add_rule_data(iAuditFD, pRule, AUDIT_FILTER_EXIT, AUDIT_ALWAYS );

	if (iRetval < 0)
	{
		if (errno == EEXIST)
		{
			cout << "rule for path ["<< rDirectory << "] already exists." << endl;//spk
			return true;
		}

		if (((errno == EINVAL) || (errno == EAGAIN)) && pRule->fields[0] == AUDIT_DIR) 
		{
			pRule->fields[0] = AUDIT_WATCH;
			iRetval = audit_add_rule_data(iAuditFD, pRule, AUDIT_FILTER_EXIT, AUDIT_ALWAYS );

			if (iRetval < 0)
			{
				cout << "Failed - sending rule data request - legacy kernel. errorno [" << errno << "]" << endl;
				return false;
			}
		}
		else
		{
			cout << "Failed - sending rule data request. errorno [" << errno << "]" << endl;
			return false;
		}
	}

	return true;
}


void AuditCB(EV_P_ ev_io * pW, int iEvents)
{

	struct audit_reply* oAuditReply = new audit_reply();
	int iRetval = audit_get_reply(gi_AuditFD, oAuditReply, GET_REPLY_NONBLOCKING, 0);

	if (iRetval > 0)
	{
		if (oAuditReply->type == AUDIT_CWD)
		{
			cout << oAuditReply->message << endl;
			
		}
		else if (oAuditReply->type == AUDIT_PATH)
		{
			cout << oAuditReply->message << endl;

		}
		else if (oAuditReply->type == AUDIT_SYSCALL)
		{
			cout << oAuditReply->message << endl;

		}
		//DMS_LOG_INFO << oAuditReply->message;
	}

	delete oAuditReply;

}



int main()
{
	string path = "";

	cout << "Enter the path you want to monitor for write event" << endl;
	cin >> path;
	cout << "setting up the audit to monitor the path ["<< path << "]" << endl;

	gi_AuditFD = audit_open();

	if (gi_AuditFD < 0)
		cout << "can not open connection to the audit kernal module" << endl;
	else if (checkAuditReadiness(gi_AuditFD) < 0)
	{

		cout << "audit is not ready" << endl;
	}
	else
	{
		cout << "audit is ready" << endl;
		sendRuleData(gi_AuditFD, path);

	}


	struct sockaddr_nl oSAddr;
	int iRetVal = -1;

	memset(&oSAddr, 0, sizeof(oSAddr));
	oSAddr.nl_family = AF_NETLINK;
	oSAddr.nl_pad = 0;
	oSAddr.nl_pid = getpid();
	oSAddr.nl_groups = AUDIT_NLGRP_READLOG;

	iRetVal = bind(gi_AuditFD, (struct sockaddr *)&oSAddr, sizeof(oSAddr));
	if (iRetVal != 0) 
	{
		cout << "Failed - binding to kernel multicast address" << endl;
	}

	//for unicast
	// if (audit_set_pid(AuditDirectoryWatcher::i_AuditFD, getpid(), WAIT_YES) < 0)
	// {
	// 	DMS_LOG_ERROR << "Failed  - set pid to the kernel";
	// }

	struct ev_loop* pLoop = ev_default_loop(EVFLAG_NOENV);
	struct ev_io oEvIO;
	ev_io_init(&oEvIO, AuditCB, gi_AuditFD, EV_READ);
	ev_io_start(pLoop, &oEvIO);

	ev_loop(pLoop, 0);


	cout << "End" << endl;

	return 0;
}
