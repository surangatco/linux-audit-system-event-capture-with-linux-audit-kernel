
#find_package(PkgConfig)
#pkg_check_modules(PC_AUDIT, QUIET,libaudit)
#set(AUDIT_DEFINITIONS, ${PC_AUDIT_CFLAGS_OTHER})


find_path(LibAudit_INCLUDE_PATH NAMES libaudit.h)
find_library(LibAudit_LIBRARY NAMES audit)

if(LibAudit_INCLUDE_PATH AND LibAudit_LIBRARY)
  set(LibAudit_FOUND TRUE)
endif(LibAudit_INCLUDE_PATH AND LibAudit_LIBRARY)

if(LibAudit_FOUND)
  if(NOT LibAudit_FIND_QUIETLY)
    message(STATUS "Found LibAudit: ${LibAudit_LIBRARY}")
  endif(NOT LibAudit_FIND_QUIETLY)
else(LibAudit_FOUND)
  if(LibAudit_FIND_REQUIRED)
    message(FATAL_ERROR "Could not find libaudit library.")
  endif(LibAudit_FIND_REQUIRED)
endif(LibAudit_FOUND)
