
#find_package(PkgConfig)
#pkg_check_modules(PC_AUDIT, QUIET,libaudit)
#set(AUDIT_DEFINITIONS, ${PC_AUDIT_CFLAGS_OTHER})


find_path(LibEvent_INCLUDE_PATH NAMES ev.h)
find_library(LibEvent_LIBRARY NAMES ev)

if(LibEvent_INCLUDE_PATH AND LibEvent_LIBRARY)
  set(LibEvent_FOUND TRUE)
endif(LibEvent_INCLUDE_PATH AND LibEvent_LIBRARY)

if(LibEvent_FOUND)
  if(NOT LibEvent_FIND_QUIETLY)
    message(STATUS "Found LibEvent: ${LibEvent_LIBRARY}")
  endif(NOT LibEvent_FIND_QUIETLY)
else(LibEvent_FOUND)
  if(LibEvent_FIND_REQUIRED)
    message(FATAL_ERROR "Could not find libevent library.")
  endif(LibEvent_FIND_REQUIRED)
endif(LibEvent_FOUND)
