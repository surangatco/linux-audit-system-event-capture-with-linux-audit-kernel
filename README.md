Want to capture sytem event such as user login, file read/write, process crash etc... ?
here it is. you can use linux audit subsystem for most of the system event monitoring...

it's two subsystems that do the  trick. one kernel module and other user space deamon.
you can use whole subsystem for your needs.
but going beyond... you might need to get into the middle of those two. i.e you can write your own application 
to communicate with the kerner module, configure it and get work done as per your requirements.
And this application of yours can work alongside with the original audit userspace deamon. In the same system.


The sample code provides how you can monitor changes to a particular file in the system.


The adventure is not over yet. you can do much more once you know the Audit !


how to build

dependencies - libaudit, libev, audit

install the dependancies via your package manager (apt-get, yum etc...)

build using cmake - 

clone the repo and go to build dir

execute  "cmake ../"

execute "make"

execute ./audit_client

that's all !

note - you should runt the app with admin previlege(as root) as it is required to set audit rules.

