cmake_minimum_required (VERSION 2.8.8)
project(audit_client CXX)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake/modules/")
set(CXX_FLAGS_DEBUG "-g3 -O0 -rdynamic -Wall -std=c++11")
set(CXX_FLAGS_RELEASE "-O3 -std=c++11")

set(CMAKE_CXX_FLAGS ${CXX_FLAGS_RELEASE})
set(CMAKE_CXX_FLAGS_DEBUG ${CXX_FLAGS_DEBUG})


set(CMAKE_EXE_LINKER_FLAGS    "-Wl,--as-needed ${CMAKE_EXE_LINKER_FLAGS}")
set(CMAKE_SHARED_LINKER_FLAGS "-Wl,--as-needed ${CMAKE_SHARED_LINKER_FLAGS}")

find_package(LibAudit REQUIRED)
find_package(LibEvent REQUIRED)

set(LIBS ${LIBS} ${LibAudit_LIBRARY})
set(LIBS ${LIBS} ${LibEvent_LIBRARY})

add_executable(audit_client "${CMAKE_CURRENT_SOURCE_DIR}/main.cc")
target_link_libraries( audit_client ${LIBS})


